#!/bin/bash
set -e
cd $(dirname $0)

network_name=dacd8

#=======================================================================
# Create docker network if necessary
#=======================================================================
if ! docker network ls | grep -q $network_name ; then
  docker network create $network_name
fi

#=======================================================================
# Start MySQL
#=======================================================================
if ! docker ps | awk '{print $2}' | grep -q mysql:5.6 ; then
  docker run -d --env-file local/local.env -p 3306:3306 \
  --network $network_name --network-alias mysql \
  -v /data/docker/dacd8_mysql:/var/lib/mysql \
  -e MYSQL_ALLOW_EMPTY_PASSWORD=yes \
  mysql:5.6
fi
docker run -it --rm --env-file local/local.env --network $network_name mysql_ready_poller

#=======================================================================
# Site install and Drush update
#=======================================================================
docker run -it --rm --env-file local/local.env \
  --network $network_name \
  -v $(pwd)/webroot:/var/www/html \
  dacd8_web scripts/site_install.sh

#Site Install does this - don't want this locally
chmod 755 webroot/sites/default

docker run -it --rm --env-file local/local.env \
  --network $network_name \
  -v $(pwd)/webroot:/var/www/html \
  dacd8_web scripts/drush_update.sh

#=======================================================================
# Run the web app: mounts are using volume flags to leverage some
# performance increases for mac around osxfs. See:
# https://docs.docker.com/docker-for-mac/osxfs-caching/#consistent
#=======================================================================
docker run -it --rm --env-file local/local.env -p 80:80 \
  --network $network_name \
  -v $(pwd)/webroot:/var/www/html:cached \
  dacd8_web
