FROM php:7.1.5-apache

RUN apt-get update \
 && apt-get upgrade -y

#===============================================================================
# Install dependencies
#===============================================================================
RUN apt-get update \
 && apt-get install -y \
      libjpeg-dev \
      libpng12-dev \
      libpq-dev \
      mysql-client \
      python-pip

RUN docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
 && docker-php-ext-install gd opcache pdo pdo_mysql zip

#===============================================================================
# Settings & Config
#===============================================================================
RUN a2enmod rewrite \
 && echo "mysql.allow_persistent = 0"           >> /usr/local/etc/php/conf.d/php_customize.ini \
 && echo "opcache.fast_shutdown = 1"            >> /usr/local/etc/php/conf.d/php_customize.ini \
 && echo "opcache.interned_strings_buffer = 8"  >> /usr/local/etc/php/conf.d/php_customize.ini \
 && echo "opcache.max_accelerated_files = 4000" >> /usr/local/etc/php/conf.d/php_customize.ini \
 && echo "opcache.memory_consumption = 128"     >> /usr/local/etc/php/conf.d/php_customize.ini \
 && echo "opcache.revalidate_freq = 0"          >> /usr/local/etc/php/conf.d/php_customize.ini \
 && echo "opcache.validate_timestamps = 1"      >> /usr/local/etc/php/conf.d/php_customize.ini

ENV DRUPAL_ACCOUNT_NAME admin
ENV DRUPAL_ACCOUNT_PASS admin
ENV DRUPAL_SITE_NAME dacd8
ENV DRUPAL_SITE_UUID 3bf09df7-0b9b-4a50-a864-bf2b9e6d09a8

#===============================================================================
# Code
#===============================================================================
WORKDIR /var/www/html

#===============================================================================
# Startup
#===============================================================================
# Need to be set locally vi env file and by environment variables in Beanstalk
# DB_APP_PASS
# DB_HOST
# DB_SCHEMA
# HASH_SALT