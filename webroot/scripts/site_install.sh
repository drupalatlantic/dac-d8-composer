#!/bin/bash
set -e

if ! echo $DB_SCHEMA | grep -Pq '^[A-Za-z0-9_]+$' ; then
  echo "DB_SCHEMA must contain only alphanumeric characters and underscores."
  exit 2
fi

source $(dirname $0)/kms_decrypt.sh

export DB_APP_PASS=$(check_and_kms_decrypt $DB_APP_PASS)
export DB_ROOT_PASS=$(check_and_kms_decrypt $DB_ROOT_PASS)
export DRUPAL_ACCOUNT_PASS=$(check_and_kms_decrypt $DRUPAL_ACCOUNT_PASS)

mysql_login="mysql -h $DB_HOST -P 3306 -u root"
if [ -n "$DB_ROOT_PASS" ] ; then
  mysql_login+=" -p$DB_ROOT_PASS"
fi

mysql_command="$mysql_login -e \"grant all privileges on $DB_SCHEMA.* to drupaluser@'%' identified by '$DB_APP_PASS'; flush privileges;\"";
eval "$mysql_command"

mysql_command="$mysql_login --silent --skip-column-names -e 'show schemas;'"
schema_list=$(eval $mysql_command)

# Only run site install if we retrieved a schema list and our schema is
# not in that list
if [ -n "$schema_list" ] && ! echo $schema_list | grep -q $DB_SCHEMA ; then
  vendor/bin/drush site-install standard install_configure_form.enable_update_status_module=NULL -y \
    --db-url=mysql://root:${DB_ROOT_PASS}@${DB_HOST}:3306/${DB_SCHEMA} \
    --account-name=${DRUPAL_ACCOUNT_NAME} \
    --account-pass=${DRUPAL_ACCOUNT_PASS} \
    --site-name=${DRUPAL_SITE_NAME}

  vendor/bin/drush cset system.site uuid 3bf09df7-0b9b-4a50-a864-bf2b9e6d09a8 -y
  vendor/bin/drush cache-rebuild

  vendor/bin/drush ev '\Drupal::entityManager()->getStorage("shortcut_set")->load("default")->delete();'
fi
