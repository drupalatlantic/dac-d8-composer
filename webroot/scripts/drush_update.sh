#!/bin/bash
set -e

sleep 5
if [ $(ls config | grep -v empty | wc -l) -gt 0 ] ; then
  vendor/bin/drush config-import --partial -y
fi
vendor/bin/drush updatedb -y
vendor/bin/drush cache-rebuild
