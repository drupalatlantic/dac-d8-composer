check_and_kms_decrypt() {
  if [ $(echo $1 | wc -c) -gt 64 ] ; then
    echo "$1" | base64 --decode > binary
    region=$(curl --connect-timeout 1 --silent http://instance-data/latest/dynamic/instance-identity/document | grep region | awk -F\" '{print $4}')
    plaintext=$(aws --region $region --output text kms decrypt --ciphertext-blob fileb://binary --query Plaintext | base64 --decode | tr -d '\n')
    rm -f binary
    echo -n $plaintext
  else
    echo -n $1
  fi
}
