#!/bin/bash
set -e

echo "Please enter a local Host IP:"
read host_ip

echo ""

if ! php -v | grep -q Xdebug ; then
    pecl install xdebug
else
    echo "Installing Xdebug: Skipped; Xdebug is already installed."
    echo ""
fi

if [ ! -f /usr/local/etc/php/conf.d/xdebug.ini ]; then
    echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" >> /usr/local/etc/php/conf.d/xdebug.ini
    echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/xdebug.ini
    echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/xdebug.ini
    echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/xdebug.ini
    echo "xdebug.remote_autostart=1" >> /usr/local/etc/php/conf.d/xdebug.ini
    echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/conf.d/xdebug.ini
    echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/xdebug.ini
    echo "xdebug.remote_host=$host_ip" >> /usr/local/etc/php/conf.d/xdebug.ini
else
    sed -i -E "s/xdebug.remote_host.*/xdebug.remote_host=$host_ip/" /usr/local/etc/php/conf.d/xdebug.ini
fi

echo "Xdebug configuration: /usr/local/etc/php/conf.d/xdebug.ini"
cat /usr/local/etc/php/conf.d/xdebug.ini

echo ""
php -v

/etc/init.d/apache2 reload