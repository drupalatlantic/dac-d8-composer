<?php

namespace Drupal\dac_slack_invite\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SlackInvite' Block
 *
 * @Block(
 *   id = "slackinvite",
 *   admin_label = @Translation("Slack Invite"),
 * )
 */
class SlackInvite extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#markup' => '',
      '#theme' => 'dac_slack_invite',
    );
  }
}
