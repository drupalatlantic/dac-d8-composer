# Drupal Atlantic Canada D8 Site (Based on the Blue Spurs Starterkit)

Local Development is driven by Docker. Deployed to a remote server via git. 

## Git Hook Setup ##
Run the following:

```
./00_hooks.sh
```

This will prompt for a JIRA prefix.  Enter the prefix for your JIRA project (USE DAC).

## Instructions for running ##

These scripts assume you already have docker setup and running on your machine.  If not, follow these [Docker setup instructions](https://docs.docker.com/engine/installation/) first.

Once you have Docker setup, running the application should involve simply running these three scripts:

```
./01_build.sh
./02_docker_build.sh
./03_docker_run.sh
```

Once these have completed, you should have a MySQL database running in one container with data mounted under /data/<project name>_mysql and a web container running in the foreground with logs being output to your console.  Once this is complete, visit http://localhost to confirm.
