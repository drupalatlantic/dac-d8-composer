#!/bin/bash
set -a # export all variables created next
source .bashrc
set +a # stop exporting

echo "Begining Deployment"

php -v

cp -f settings.php webroot/sites/default/settings.php 

. ./01_build.sh  

cd webroot

. ./scripts/drush_update.sh

echo "Deployment Done"
