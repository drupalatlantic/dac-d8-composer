#!/bin/bash
set -e
cd $(dirname $0)

#=======================================================================
# Build docker containers
#=======================================================================
docker build -f local/Dockerfile.mysql_client -t mysql_ready_poller local
docker build -t dacd8_web .
