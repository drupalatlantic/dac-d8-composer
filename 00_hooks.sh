#!/bin/bash
echo "Please enter a JIRA prefix (eg - BHD):"
read jira_prefix

mkdir -p $(dirname $0)/.git/hooks
sed "s|%%jira_prefix%%|$jira_prefix|g" $(dirname $0)/git_hooks/commit-msg > $(dirname $0)/.git/hooks/commit-msg
chmod +x $(dirname $0)/.git/hooks/commit-msg

cp $(dirname $0)/git_hooks/pre-commit $(dirname $0)/.git/hooks/
chmod +x $(dirname $0)/.git/hooks/pre-commit

composer global require drupal/coder
composer global require "squizlabs/php_codesniffer=*"

if [ -d $HOME/.composer/vendor/drupal/coder/coder_sniffer ] ; then
  sudo phpcs --config-set installed_paths $HOME/.composer/vendor/drupal/coder/coder_sniffer
else
  sudo phpcs --config-set installed_paths $HOME/.config/composer/vendor/drupal/coder/coder_sniffer
fi
