#!/bin/bash
ret_val=1
while [ $ret_val != 0 ] ; do
  sleep 1
  mysql_command="mysql -h $DB_HOST -u root"
  if [ -n "$DB_ROOT_PASS" ] ; then
    mysql_command+=" -p$DB_ROOT_PASS"
  fi
  mysql_command+=" -e 'show schemas;'"

  eval $mysql_command
  ret_val=$?
done

