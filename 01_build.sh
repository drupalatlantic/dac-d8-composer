#!/bin/bash
set -e
cd $(dirname $0)

#=======================================================================
# Prepare docroot
#=======================================================================
cd webroot
composer install
composer drupal-scaffold
echo ""
cd ..
